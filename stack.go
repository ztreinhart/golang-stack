package stack

import (
	"sync"
)

type node struct {
	data interface{}
	next *node
}

type Stack struct {
	lock *sync.RWMutex
	head *node
	Size int
}

func New() *Stack {
	s := &Stack{}
	s.lock = &sync.RWMutex{}

	return s
}

func (s *Stack) Push(data interface{}) {
	s.lock.Lock()

	newNode := &node{}
	newNode.data = data

	temp := s.head
	newNode.next = temp
	s.head = newNode

	s.Size++

	s.lock.Unlock()
}

func (s *Stack) Pop() interface{} {
	if s.head == nil {
		return nil
	}

	s.lock.Lock()

	data := s.head.data
	s.head = s.head.next

	s.Size--

	s.lock.Unlock()

	return data
}

func (s *Stack) Peek() interface{} {
	if s.head == nil {
		return nil
	}

	s.lock.RLock()

	data := s.head.data

	s.lock.RUnlock()

	return data
}

func (s *Stack) Clear() {
	s.lock.Lock()

	s.head = nil
	s.Size = 0

	s.lock.Unlock()
}
